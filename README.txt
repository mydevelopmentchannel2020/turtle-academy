This repository provides examples for Turtle Academy Lessons.

https://turtleacademy.com/

Here you can find the lessons:
https://turtleacademy.com/lessons

Here is the command reference - section Basic Commands:
https://turtleacademy.com/playground

Check my YouTube channel MyDevChannel:
https://www.youtube.com/channel/UC28TiXa7e9J400XdU1WueTQ

Check playlist "Turtle Academy Lessons"
